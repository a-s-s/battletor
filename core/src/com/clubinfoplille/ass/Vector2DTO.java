package com.clubinfoplille.ass;

import lombok.Value;

@Value
public class Vector2DTO {
    float x, y;
}
