package com.clubinfoplille.ass;

import com.badlogic.gdx.physics.box2d.Body;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

public class MissileEntity {
    @JsonIgnore
    @Getter
    @Setter
    private Body body;


    private PlayerCommandDTO command;

    public MissileEntity(Body body) {
        this.body = body;
    }

    public void setCommand(PlayerCommandDTO command) {
        // logic in the controls, like max, inertia, fuel etc.
        if (command.getThrottle() < 0)
            command.setThrottle(0);
        else if (command.getThrottle() > 1)
            command.setThrottle(1);

        if (command.getWing() < -1)
            command.setWing(-1);
        else if (command.getWing() > 1)
            command.setWing(1);

        this.command = command;
    }

    public void newFrameApplyControls() {
        if (body != null) {
        /*
        Interesting documentation :
        https://github.com/jbox2d/jbox2d/blob/master/jbox2d-library/src/main/java/org/jbox2d/dynamics/Body.java
        https://web.archive.org/web/20120229233701/http://paulbourke.net/geometry/polyarea/
         */

            body.applyForce(
                    (float) (5.0f * command.getThrottle() * Math.sin(command.getWing()*Math.PI/8) + body.getAngle()), //max angle set arbitrarily, may want to change it
                    (float) (5.0f * command.getThrottle() * Math.cos(command.getWing()*Math.PI/8) + body.getAngle()),
                    (float) (body.getWorldCenter().x + 0.66f * Math.sin(body.getAngle())), //we apply the force at the bottom of the missile
                    (float) (body.getWorldCenter().y - 0.66f * Math.cos(body.getAngle())),
                    true);
        }
    }
}
