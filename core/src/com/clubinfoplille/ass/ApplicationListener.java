package com.clubinfoplille.ass;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.utils.ScreenUtils;

import java.io.IOException;

public class ApplicationListener extends ApplicationAdapterSafe {

    private Kernel kernel;

    private Box2DDebugRenderer debugRenderer;

    @Override
    public void createSafe() throws IOException {
        setupTestDebuggerView();
    }

    private void setupTestDebuggerView() throws IOException {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);

        float ratio = (float) (Gdx.graphics.getWidth()) / (float) (Gdx.graphics.getHeight());

        kernel = new Kernel();
        debugRenderer = new Box2DDebugRenderer();
    }

    @Override
    public void renderSafe() throws IOException {
        ScreenUtils.clear(Color.SLATE);

        kernel.getStage().act();
        kernel.getStage().draw();

        // debug only
        debugRenderer.render(kernel.getWorld(), kernel.getStage().getCamera().combined);

        kernel.newFrame();
    }

    @Override
    public void dispose() {
    }
}
