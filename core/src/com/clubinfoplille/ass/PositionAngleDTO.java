package com.clubinfoplille.ass;

import com.badlogic.gdx.math.Vector2;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;

@Value
public class PositionAngleDTO {
    float x, y, angle;

    public PositionAngleDTO(Vector2 position, float angle) {
        this.x = position.x;
        this.y = position.y;
        this.angle = angle;
    }
}
