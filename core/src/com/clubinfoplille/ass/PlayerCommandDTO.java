package com.clubinfoplille.ass;

import lombok.*;

@Data
public class PlayerCommandDTO {
    private float throttle, wing;
}
