package com.clubinfoplille.ass;


import lombok.Value;

@Value
public class WorldUpdateDTO {
    PositionAngleDTO userMissile, enemyMissile;
    Vector2DTO targetPosition;

}
