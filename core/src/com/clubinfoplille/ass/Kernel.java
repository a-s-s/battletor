package com.clubinfoplille.ass;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.io.IOException;

import static com.clubinfoplille.ass.Kernel.KernelState.*;

public class Kernel {

    private final Stage stage;
    private final World world;
    private final Server server;

    private MissileEntity userMissile;
    private MissileEntity attackingMissile;
    private final InitialCondition initialCondition;
    //TODO check if need target entity ??

    enum KernelState {
        WAITING_FOR_CLIENTS,
        SEND_SIM_STATE,
        UPDATE_SIM_STATE,
        WAIT_FOR_ORDER,
        END_OF_SIM
    }

    private KernelState kernelState = WAITING_FOR_CLIENTS;

    public Kernel() throws IOException {
        server = new Server("localhost", 8080, 1);

        Box2D.init();
        world = new World(new Vector2(0, -10), true);
        world.setContactListener(new CrashListener());

        stage = new Stage(new ScreenViewport());
        stage.getCamera().position.set(0, 0, 10);
        stage.getCamera().lookAt(0, 0, 0);
        stage.getCamera().viewportWidth = 20;
        stage.getCamera().viewportHeight = 20;

        // To be retrieve at the startup of the app
        initialCondition = InitialCondition.builder()
                .targetPosition(new Vector2(5, -8))
                .attackingMissilePosition(new Vector2(-9, 4))
                .userMissilePosition(new Vector2(0, -8))
                .build();

        setupWorldBox();
        setupTarget(initialCondition.getTargetPosition());
        setupAttackingMissile(initialCondition.getAttackingMissilePosition());
        setupUserMissile(initialCondition.getUserMissilePosition());

    }

    private void setupWorldBox() {
        setupGround(-9, 11);

        setupSkyLimit(new Vector2(-11, 13), new Vector2(-11, -9));
        setupSkyLimit(new Vector2(11, 13), new Vector2(-11, 13));
        setupSkyLimit(new Vector2(11, -9), new Vector2(11, 13));
    }

    private void setupGround(float height, float width) {// add height ?, add rest of the field ?
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(0, 0); // 0,0 = center

        Body body = world.createBody(bodyDef);
        body.setUserData("ground");

        EdgeShape groundShape = new EdgeShape();
        groundShape.set(-width, height, width, height);

        body.createFixture(getDefaultFixtureDef(groundShape));

        groundShape.dispose();
    }

    private void setupSkyLimit(Vector2 p1, Vector2 p2) {// add height ?, add rest of the field ?
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(0, 0); // 0,0 = center

        Body body = world.createBody(bodyDef);
        body.setUserData("ground"); //could differentiate from ground

        EdgeShape groundShape = new EdgeShape();
        groundShape.set(p1, p2);

        body.createFixture(getDefaultFixtureDef(groundShape));

        groundShape.dispose();
    }

    private void setupTarget(Vector2 targetPosition) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(targetPosition);
        bodyDef.linearVelocity.set(2f, 10f);

        Body body = world.createBody(bodyDef);
        body.setUserData("target");

        SquareShape missileShape = new SquareShape(1.0f);

        //TODO need a fixture ?
        Fixture fixture = body.createFixture(getDefaultFixtureDef(missileShape));

        missileShape.dispose();
    }

    private void setupAttackingMissile(Vector2 attackingMissilePosition) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(attackingMissilePosition); // 0,0 = center
        bodyDef.linearVelocity.set(5f, 10f);

        Body body = world.createBody(bodyDef);
        body.setUserData("attackingMissile");

        MissileShape missileShape = new MissileShape(2.0f, 0.5f);

        Fixture fixture = body.createFixture(getDefaultFixtureDef(missileShape));

        attackingMissile = new MissileEntity(body);

        missileShape.dispose();
    }

    private void setupUserMissile(Vector2 userMissilePosition) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(userMissilePosition); // 0,0 = center
        bodyDef.linearVelocity.set(0, 0);
        bodyDef.angularDamping = 0;

        Body body = world.createBody(bodyDef);
        body.setUserData("userMissile");

        MissileShape missileShape = new MissileShape(1.1f, 0.3f);

        Fixture fixture = body.createFixture(getDefaultFixtureDef(missileShape));

        userMissile = new MissileEntity(body);

        missileShape.dispose();
    }

    private FixtureDef getDefaultFixtureDef(Shape shape) {
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 0.5f;
        fixtureDef.friction = 0.4f;
        fixtureDef.restitution = 0.6f; // Make it bounce a little bit
        return fixtureDef;
    }

    private boolean periodIsPassed() {
        // LATER can cut sim in period of time
        // to be done

        //for now, new control from user is needed for each frame
        return false;
    }

    private boolean isSimulationStillRunning() {
        if (CrashListener.isUserDestroyed && userMissile.getBody() != null) {
            world.destroyBody(userMissile.getBody());
            userMissile.setBody(null);
            return false;
        }

        if (attackingMissile.getBody() == null || userMissile == null)
            return false;
        return true;
    }


    private KernelState receiveNewOrder() throws IOException {
        PlayerCommandDTO result = server.readMessage();

        // check if a new order is received
        if (result == null) {
            return WAIT_FOR_ORDER;
        }


        if (periodIsPassed())
            return WAIT_FOR_ORDER;

        userMissile.setCommand(result);
        return UPDATE_SIM_STATE;
    }

    public void newFrame() throws IOException {
        switch (kernelState) {
            case WAITING_FOR_CLIENTS:
                server.pollClients();
                kernelState = server.isReadyToBegin() ? SEND_SIM_STATE : WAITING_FOR_CLIENTS;
                return;
            case SEND_SIM_STATE:
                server.sendWorldUpdate(userMissile, attackingMissile, initialCondition.getTargetPosition());
                kernelState = WAIT_FOR_ORDER;
                return;
            case UPDATE_SIM_STATE:
                if (isSimulationStillRunning()) {
                    userMissile.newFrameApplyControls();
                    world.step(1 / 60f, 6, 2);
                    kernelState = SEND_SIM_STATE;
                }
                else
                    kernelState = END_OF_SIM;
                return;

            case WAIT_FOR_ORDER:
                kernelState = receiveNewOrder();
                return;
            case END_OF_SIM:
                exitGracefully();
                //return;
        }
    }

    // debug only method
    public World getWorld() {
        return world;
    }

    // debug only method
    public Stage getStage() {
        return stage;
    }

    public void exitGracefully() {
        Gdx.app.log("KERNEL", "Kernel shutting down...");

        if (server != null) {
            try {
                server.closeAllSockets();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Gdx.app.exit();
    }
}
