package com.clubinfoplille.ass;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.*;

public class CrashListener implements ContactListener {

    public static boolean isUserDestroyed = false;

    @Override
    public void beginContact(Contact contact) {
        Fixture fa = contact.getFixtureA();
        Fixture fb = contact.getFixtureB();
        Gdx.app.debug("PHYSIC",fb.getBody().getUserData() + " has hit " + fa.getBody().getUserData());

        if (oneIs("attackingMissile", fa, fb)) {
            if (oneIs("userMissile", fa, fb)) {
                ApplicationInputOutput.endOfSim(ApplicationInputOutput.EndState.SUCCEED);
            } else if (oneIs("ground", fa, fb)) {
                ApplicationInputOutput.endOfSim(ApplicationInputOutput.EndState.SUCCEED);
            } else if (oneIs("target", fa, fb)) {
                ApplicationInputOutput.endOfSim(ApplicationInputOutput.EndState.FAILED);
            } else {
                throw new RuntimeException("Unreachable code");
            }
        } else if (oneIs("userMissile", fa, fb)) {
            if (oneIs("target", fa, fb)) {
                ApplicationInputOutput.endOfSim(ApplicationInputOutput.EndState.FAILED);
            }

            isUserDestroyed = true;
        }

    }

    private boolean oneIs(String comparisonUserData, Fixture fa, Fixture fb) {
        return fa.getBody().getUserData() == comparisonUserData || fb.getBody().getUserData() == comparisonUserData;
    }

    @Override
    public void endContact(Contact contact) {
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
    }
}
