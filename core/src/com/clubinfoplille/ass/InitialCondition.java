package com.clubinfoplille.ass;

import com.badlogic.gdx.math.Vector2;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class InitialCondition {

    private Vector2 attackingMissilePosition;
    //angle
    //speed
    //size
    //power
    //...

    private Vector2 userMissilePosition;
    //same as attackingMissile


    private Vector2 targetPosition;
    //maybe size


}
