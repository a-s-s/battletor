package com.clubinfoplille.ass;

import com.badlogic.gdx.ApplicationAdapter;

public abstract class ApplicationAdapterSafe extends ApplicationAdapter {

    public abstract void createSafe() throws Exception;

    @Override
    public void create() {
        try {
            createSafe();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public abstract void renderSafe() throws Exception;

    @Override
    public void render() {
        try {
            renderSafe();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
