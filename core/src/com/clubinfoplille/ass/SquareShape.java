package com.clubinfoplille.ass;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class SquareShape extends PolygonShape {

    SquareShape(float edgeLength) {
        super();
        this.set(new Vector2[]{
                new Vector2(0, 0),
                new Vector2(edgeLength, 0),
                new Vector2(edgeLength, edgeLength),
                new Vector2(0, edgeLength)
        });
    }
}
