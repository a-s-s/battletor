package com.clubinfoplille.ass;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class MissileShape extends PolygonShape {

    private static final float DEFAULT_HEIGHT = 1.0f;
    private static final float DEFAULT_WIDTH = 1.0f;
    private static final float DEFAULT_FAIRING_HEIGHT = 0.6f;

    MissileShape() {
        this(DEFAULT_HEIGHT, DEFAULT_WIDTH);
    }

    MissileShape(float height, float width) {
        this(height, width, DEFAULT_FAIRING_HEIGHT);
    }

    MissileShape(float height, float width, float fairingHeight) {
        super();
        this.set(new Vector2[]{
                //left
                new Vector2(0.0f, 0.0f),
                new Vector2(0.0f, height),
                //center
                new Vector2(width / 2, height + fairingHeight),
                //right
                new Vector2(width, height),
                new Vector2(width, 0.0f),
        });
    }
}
