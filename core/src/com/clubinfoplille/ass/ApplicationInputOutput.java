package com.clubinfoplille.ass;

import com.badlogic.gdx.Gdx;

public class ApplicationInputOutput {

    public enum EndState {
        UNDEFINED,
        FAILED,
        SUCCEED
    }

    public static void endOfSim(EndState endState) {
        Gdx.app.log("APPLICATION", "App finished, output state : " + endState.toString());
        Gdx.app.exit();
    }
}
