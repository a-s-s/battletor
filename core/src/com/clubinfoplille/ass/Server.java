package com.clubinfoplille.ass;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.*;

public class Server {
    private final Selector selector;
    private final ServerSocketChannel serverSocketChannel;
    private final int maxClients;
    private final ObjectMapper objectMapper;
    private int connectedClients;

    @Getter
    private boolean isReadyToBegin;

    public Server(String ipAddress, int port, int maxClients) throws IOException {
        InetAddress host = InetAddress.getByName(ipAddress);
        selector = Selector.open();
        serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.bind(new InetSocketAddress(host, port));
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        this.maxClients = maxClients;
        this.connectedClients = 0;
        this.isReadyToBegin = false;
        Gdx.app.debug("SERVER", "Serveur créé");
        objectMapper = new ObjectMapper();

    }

    private boolean isNoRequestPending() throws IOException {
        return selector.select(1) <= 0;
    }

    public void pollClients() throws IOException {
        SelectionKey key;
        if (isNoRequestPending())
            return;
        Set<SelectionKey> selectedKeys = selector.selectedKeys();
        Iterator<SelectionKey> iterator = selectedKeys.iterator();
        while (iterator.hasNext()) {
            key = iterator.next();
            iterator.remove();

            if (connectedClients < maxClients && key.isAcceptable()) {
                SocketChannel sc = serverSocketChannel.accept();
                sc.configureBlocking(false);
                sc.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
                Gdx.app.debug("SERVER","Connection Accepted: " + sc.getLocalAddress());
                connectedClients++;
            }
        }

        if (connectedClients == maxClients)
            isReadyToBegin = true;
    }

    public PlayerCommandDTO readMessage() throws IOException {
        SelectionKey key;
        if (isNoRequestPending())
            return null;
        Set<SelectionKey> selectedKeys = selector.selectedKeys();
        Iterator<SelectionKey> iterator = selectedKeys.iterator();
        while (iterator.hasNext()) {
            key = iterator.next();
            iterator.remove();

            if (key.isReadable()) {
                SocketChannel sc = (SocketChannel) key.channel();
                ByteBuffer bb = ByteBuffer.allocate(1024);
                sc.read(bb);
                String result = new String(bb.array()).trim();
                //Gdx.app.debug("SERVER", result);


                PlayerCommandDTO playerCommandDTO = objectMapper.readValue(result, PlayerCommandDTO.class);
                if (result.length() <= 0) {
                    closeSocket(sc);
                }

                return playerCommandDTO;
            }
        }

        return null;
    }

    public void closeSocket(SocketChannel socket) throws IOException {
        if (socket.isOpen()) {
            Gdx.app.debug("SERVER", "Closing socket " + socket.getRemoteAddress().toString());
            socket.close();
        }
    }

    public void closeAllSockets() throws IOException {
        if (selector.isOpen()) {
            if (isNoRequestPending())
                return;
            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectedKeys.iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                iterator.remove();
                SocketChannel sc = (SocketChannel) key.channel();
                closeSocket(sc);
            }
            selector.close();
            Gdx.app.debug("SERVER", "Connection closed...");
        }
    }

    public void sendWorldUpdate(MissileEntity userMissile, MissileEntity enemyMissile, Vector2 targetPosition) throws IOException {
        SelectionKey key;
        if (isNoRequestPending())
            return;
        Set<SelectionKey> selectedKeys = selector.selectedKeys();
        Iterator<SelectionKey> iterator = selectedKeys.iterator();
        while (iterator.hasNext()) {
            key = iterator.next();
            iterator.remove();

            if (key.isWritable()) {
                WorldUpdateDTO worldUpdateDTO = new WorldUpdateDTO(
                        new PositionAngleDTO(userMissile.getBody().getPosition(), userMissile.getBody().getAngle()),
                        new PositionAngleDTO(enemyMissile.getBody().getPosition(), enemyMissile.getBody().getAngle()),
                        new Vector2DTO(targetPosition.x, targetPosition.y));
                String msg = objectMapper.writeValueAsString(worldUpdateDTO);
                SocketChannel sc = (SocketChannel) key.channel();
                ByteBuffer bb = ByteBuffer.wrap(msg.getBytes());
                sc.write(bb);
            }
        }
    }
}

